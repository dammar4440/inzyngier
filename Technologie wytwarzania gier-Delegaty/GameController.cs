﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEditor.Analytics;
using UnityEngine.Analytics;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
	public string bestScoreName = "BestScore";
    public float startTime = 0;
    public float time = 0;
    public int lifes = 5;
    public int score = 0;
    public int bestScore = 0;
    public static UnityAction Damage;
    public static UnityAction<float> UpTimeUI;
    public static UnityAction<int> UpScoreUI;
    public static UnityAction<int> UpLifeUI;
    public static UnityAction<int> UpBestUI;
    public int nextScene;
    private void Awake()
    {
        Instance = this;
        startTime = Time.time;
        bestScore = PlayerPrefs.GetInt(bestScoreName, 0);
        UpBestUI?.Invoke(bestScore);
        UpLifeUI?.Invoke(lifes);
    }
    public void IncreasePoints()
    {
        score++;
        UpScoreUI?.Invoke(score);
    }
    public void DecreaseLifes()
    {
        lifes--;
        UpLifeUI?.Invoke(lifes);
        DamageFade?.Invoke();
        if (lifes == 0)
        {
            GameOver();
        }
    }
    public void GameOver()
    {
        if (score > bestScore)
        {
            PlayerPrefs.SetInt(bestScoreName, score);
        }
        SceneManager.LoadScene(0);
    }
    private void Update()
    {
        UpTimeUI?.Invoke(time);
    }
}
