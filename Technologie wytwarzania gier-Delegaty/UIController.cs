﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject damage;
    [SerializeField] TextMeshProUGUI timeValue;
    [SerializeField] TextMeshProUGUI scoreValue;
    [SerializeField] TextMeshProUGUI lifesValue;
    [SerializeField] TextMeshProUGUI bestScoreValue;
    void Awake()
    {
        GameController.Damage += OnDamagePanel;
        GameController.UpTimeUI += TimeUp;
        GameController.UpScoreUI += ScoreUp;
        GameController.UpLifeUI += LifeUp;
        GameController.UpBestUI += BestUp;
    }
    void OnDamagePanel()
    {
        Damage.SetActive(true);
        Invoke("Damage", 0.10f);
    }
    void TimeUp(float time)
    {
        timeValue.text = "Time: " + TimeSpan.FromSeconds(time).ToString("mm\\:ss");
    }
    void ScoreUp(int score)
    {
        scoreValue.text = "Score: " + score;
    }
    void LifeUp(int lifes)
    {
        lifesValue.text = "Life: " + lifes;
    }
    void BestUp(int bestScore)
    {
        bestScoreValue.text = "Best score: " + bestScore;
    }
    private void OnDestroy()
    {
        GameController.Damage -= OnDamagePanel;
        GameController.UpTimeUI -= TimeUp;
        GameController.UpScoreUI -= ScoreUp;
        GameController.UpLifeUI -= LifeUp;
        GameController.UpBestUI -= BestUp;
    }
}
