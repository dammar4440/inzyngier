﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    int lifes = 3;
    int score = 0;
    int bestScore = 0;
    float startTime = 0;
    float time = 0;

    [SerializeField]
    GameObject damageFade;
    [SerializeField]
    TextMeshProUGUI timeValue;
    [SerializeField]
    TextMeshProUGUI scoreValue;
    [SerializeField]
    TextMeshProUGUI bestScoreValue;
    [SerializeField]
    TextMeshProUGUI lifesValue;

    //public static UnityAction<int> OnPointsChange;
    //public static UnityAction<int> OnPointsChange;
    //public static UnityAction<int> OnPointsChange;
    // public static UnityAction<int> OnPointsChange;
    //public static UnityAction<int> OnPointsChange;

    [SerializeField]
    int nextScene;

    string bestScoreName = "bestScore";
    string timeFormat = "mm\\:ss";

    public int Points { get => score; set => score = value; }

    private void Awake()
    {
		AnalyticsResult result = Analytics.CustomEvent("Nowa sesja");
        Instance = this;
        startTime = Time.time;
        bestScore = PlayerPrefs.GetInt(bestScoreName, 0);
        bestScoreValue.text = bestScore.ToString();
        lifesValue.text = lifes.ToString();
        
        SceneManager.LoadScene(0);
        scoreValue.text = score.ToString();
        result = AnalyticsEvent.Custom("event_With_Data", new Dictionary<string, object>
        {
            {"event_id",1 },
            {"best",bestScore }
        });

        Debug.Log(result);
        // Debug.Log(result);
        // 
        //Debug.Log(result);

    }
    private void Update()
    {
        timeValue.text = TimeSpan.FromSeconds(Time.time - startTime).ToString(timeFormat);
    }
    public void IncreasePoints()
    {
        score++;
        scoreValue.text = score.ToString();
    }
    public void DecreaseLifes()
    {
        lifes--;
        lifesValue.text = lifes.ToString();
        damageFade.SetActive(true);
        Invoke("TurnOffDamageFade", 0.25f);
        if (lifes == 0)
        {
            GameOver();
            
            
        }
    }
    void TurnOffDamageFade()
    {
        damageFade.SetActive(false);
    }

    public void GameOver()
    {
		
        if(score>bestScore)
        {
            AnalyticsResult result2 = AnalyticsEvent.Custom("Najlepszy wynik/ Wynik", new Dictionary<string, object>
            {
                { "Event_id", 2},
                { "BeatenScore", bestScore},
                { "NewBestScore", score},
                { "TimePlayedInSesion",Time.time}
            });
            PlayerPrefs.SetInt(bestScoreName, score);
            
        }
        
        SceneManager.LoadScene(0);
    }
    public void OnApplicationQuit()
    {
        AnalyticsResult result = Analytics.CustomEvent("Koniec gry");
        Debug.Log(result);
        Analytics.FlushEvents();
    }
    public void NextScene()
    {
        SceneManager.LoadScene(nextScene);
    }
}
