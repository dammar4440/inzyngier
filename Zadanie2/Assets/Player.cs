﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public GameObject youwinText, restart, blood;
    public Transform SpawnPlayer;
    private Rigidbody2D rigi;
    public float moveSpeed=5;
    public float jumpHeight = 15;
    // Use this for initialization

    void Awake()
    {
        rigi = transform.GetComponent<Rigidbody2D>();
    }

    void Start () {
        youwinText.SetActive(false);
        restart.SetActive(false);
	}
	
	// Update is called once per frame
    public void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            rigi.velocity = Vector2.up * jumpHeight; 

        }
        HandleMovement();
	}
    private void HandleMovement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigi.velocity = new Vector2(-moveSpeed, rigi.velocity.y);

        }
        else
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rigi.velocity = new Vector2(+moveSpeed, rigi.velocity.y);
                
              // kozioł ->  530 532 280

            }
            else
            {
                rigi.velocity = new Vector2(0, rigi.velocity.y);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Circle"))
        {
          //  Debug.Log("GAMEOVER");
          SceneManager.LoadScene("SampleScene");
        }
        if (collision.gameObject.CompareTag("flagyellow"))
        {
            youwinText.SetActive(true);
            restart.SetActive(true);
          //  Instantiate(blood, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("flagyellow"))
        {
            youwinText.SetActive(true);
            restart.SetActive(true);
            //  Instantiate(blood, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }

}
