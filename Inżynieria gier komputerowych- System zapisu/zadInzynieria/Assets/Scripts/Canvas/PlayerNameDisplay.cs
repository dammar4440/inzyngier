﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameDisplay : MonoBehaviour
{
    Text nameText;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        nameText = GetComponent<Text>();
        gameController = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        nameText.text = gameController.GetName();
    }
}
