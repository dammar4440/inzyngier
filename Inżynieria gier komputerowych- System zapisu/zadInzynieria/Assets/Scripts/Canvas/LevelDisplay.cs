﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDisplay : MonoBehaviour
{
    Text levelText;
    GameController gameController;
    void Start()
    {
        levelText = GetComponent<Text>();
        gameController = FindObjectOfType<GameController>();
    }
    void Update()
    {
        levelText.text = gameController.GetLevel().ToString();
    }
}
