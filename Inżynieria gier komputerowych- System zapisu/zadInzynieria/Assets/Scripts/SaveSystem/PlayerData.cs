﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public float positionX;
    public float positionY;
    public float positionZ;
    public float Health;
    public int Level;
    public String Name;

    public PlayerData(PlayerController3D controller)
    {
        positionX = controller.transform.position.x;
        positionY = controller.transform.position.y;
        positionZ = controller.transform.position.z;
        Name = controller.stats.Name;
        Level = controller.stats.Level;
        Health = controller.stats.health;
    }

    public PlayerData()
    {
        positionX = 0;
        positionY = 0;
        positionZ = 0;
        Name = "Player";
        Level = 1;
        Health = 100f;
    }
    public PlayerData(string name)
    {
        positionX = 0;
        positionY = 0;
        positionZ = 0;
        Name = name;
        Level = 1;
        Health = 100f;
    }
}
