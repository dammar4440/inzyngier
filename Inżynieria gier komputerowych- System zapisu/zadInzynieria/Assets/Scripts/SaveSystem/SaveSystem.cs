﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveGame(PlayerController3D player)
    {
        string path = Application.streamingAssetsPath + "/" + player.stats.Name + ".save";
        PlayerData data = new PlayerData(player);
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, data);
        stream.Close();
    }
    private static void SaveNewPlayer(PlayerData data)
    {
        string path = Application.streamingAssetsPath + "/" + data.Name + ".save";
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, data);
        stream.Close();
    }
    public static PlayerData LoadGame(string name)
    {
        string path = Application.streamingAssetsPath + "/" + name + ".save";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PlayerData data = (PlayerData)formatter.Deserialize(stream);
            stream.Close();
            return data;
        }
        else
        {
            PlayerData data = new PlayerData();
            SaveNewPlayer(data);
            return data;
        }
    }
    public static void NewGame(string name)
    {
        string path = Application.streamingAssetsPath + "/" + name + ".save";
        PlayerData data = new PlayerData(name);
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, data);
        stream.Close();
    }
}
